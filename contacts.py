from json import encoder
import requests 
import json
HEADERS = {
    'Authorization': ''
}
TOKEN = None

def get_contacts():
    result = requests.get('https://sdkdemo.tel4vn.com/api/v1/crm/contacts/', headers=HEADERS)
    print(json.dumps(result.json(), indent=4, ensure_ascii=False))

def get_contact(id):
    result = requests.get('https://sdkdemo.tel4vn.com/api/v1/crm/contacts/{}'.format(id), headers=HEADERS)
    print(json.dumps(result.json(), indent=4, ensure_ascii=False))

def login(username, password):
    data = {
        "username": username,
        "password": password
    }
    result = requests.post('https://sdkdemo.tel4vn.com/api/v1/auth/login/', data=data)    
    res = result.json()
    if 'token' in res:
        TOKEN = res['token']
        HEADERS['Authorization'] = 'JWT {}'.format(TOKEN)

if __name__ == '__main__':
    # get_contacts()
    # get_contact(1)
    login('toni01', 'tel4vn.com')
    get_contact(1)