import unittest
from calc import add, subtract
class TestCalcMethods(unittest.TestCase):

  def test_add(self):
    total = add(5, 2)
    self.assertEqual(total, 7)
    
  def test_subtract(self):
    res = subtract(5, 2)
    self.assertEqual(res, 3)
    
if __name__ == '__main__':
    unittest.main()