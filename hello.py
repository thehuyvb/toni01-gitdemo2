"""
This is a hello Class
"""

class Hello:
  def __init__(self):
    print("constuctor __init__")
  
  def say(self, text):
    print("Say {}".format(text))

  def greeting(self):
    text = "Hello"
    self.say(text)
    
if __name__ == '__main__':
  c = Hello()
  c.greeting()

